<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\ProcessorNull;

use Interactiv4\Contracts\Factory\Api\FactoryInterface;

/**
 * Class ProcessorNullFactory.
 *
 * @api
 */
class ProcessorNullFactory implements FactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function create(array $data = []): ProcessorNull
    {
        return new ProcessorNull();
    }
}
