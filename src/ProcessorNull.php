<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\ProcessorNull;

use Interactiv4\Contracts\DataObject\Api\DataObjectInterface;
use Interactiv4\Contracts\Processor\Api\ProcessorInterface;

/**
 * Class ProcessorNull.
 *
 * This processor can be used to avoid conditional calls.
 *
 * If no processor is provided to your library, creating a ProcessorNull instance
 * is a good way to avoid littering your code with `if ($this->processor) { }`
 * blocks.
 *
 * @api
 */
class ProcessorNull implements ProcessorInterface
{
    /**
     * Null processor.
     *
     * {@inheritdoc}
     */
    public function process(?DataObjectInterface $data = null): void
    {
        // noop;
    }
}
