<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\ProcessorNull\Api;

use Interactiv4\Contracts\Factory\Api\ObjectFactoryInterface;
use Interactiv4\Contracts\Factory\Factory\Proxy;
use Interactiv4\Contracts\Processor\Api\ProcessorInterface;
use Interactiv4\Contracts\Processor\Api\ProcessorInterfaceFactoryInterface;
use Interactiv4\ProcessorNull\ProcessorNullFactory;

/**
 * Class ProcessorNullInterfaceFactory.
 *
 * @api
 */
class ProcessorNullInterfaceFactory extends Proxy implements ProcessorInterfaceFactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function __construct(
        ObjectFactoryInterface $objectFactory,
        string $factoryClass = ProcessorNullFactory::class,
        array $factoryArguments = []
    ) {
        parent::__construct(
            $objectFactory,
            $factoryClass,
            $factoryArguments
        );
    }

    /**
     * Proxy object creation to concrete factory.
     *
     * {@inheritdoc}
     */
    public function create(array $arguments = []): ProcessorInterface
    {
        return parent::create($arguments);
    }
}
