<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\ProcessorNull\Test;

use Interactiv4\Contracts\DataObject\Api\DataObjectInterface;
use Interactiv4\Contracts\Processor\Api\ProcessorInterface;
use Interactiv4\ProcessorNull\ProcessorNull;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class ProcessorNullTest.
 *
 * @internal
 */
class ProcessorNullTest extends TestCase
{
    /**
     * Test class exists and is an instance of ProcessorInterface.
     */
    public function testInstanceOf(): void
    {
        $processor = new ProcessorNull();
        static::assertInstanceOf(ProcessorInterface::class, $processor);
    }

    /**
     * Test return value is true.
     */
    public function testReturnValueIsTrue(): void
    {
        // Check without arguments
        $processor = new ProcessorNull();
        static::assertEmpty($processor->process());

        // Check with arguments
        /** @var DataObjectInterface|MockObject $processorDataMock */
        $processorDataMock = $this->getMockForAbstractClass(DataObjectInterface::class);
        $processorDataMock->expects(static::never())->method(static::anything());

        static::assertEmpty($processor->process($processorDataMock));
    }
}
